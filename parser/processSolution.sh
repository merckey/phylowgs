#!/bin/bash
set -e
JOBS=$1
if [ ! -e $1 ];then
	echo Job file not found!
	exit 1
fi
#/home/lji226/data/coad/vcf
VCFDIR=$2

for SAMPLE in $(cat $JOBS); do
	echo ${SAMPLE}
	if [ -e ${SAMPLE}/cnv_data.txt ];then
		echo Skip ${SAMPLE}
		continue
	fi
	mkdir -p ${SAMPLE}
	gsutil cp gs://bucket1q2w/jpliu/titancna/tcga/coad/${SAMPLE}/optimalClusterSolution.txt ${SAMPLE}/
	cellularity=$(tail -n +2 ${SAMPLE}/optimalClusterSolution.txt | cut -f 6)
	echo "Cellularity = ${cellularity}"
	optimal=$(tail -n +2 ${SAMPLE}/optimalClusterSolution.txt | cut -f 11 | sed "s/\/\//\//" | sed "s/results\/titan\/hmm\///").segs.txt
	echo "Optimal Solution: ${optimal}"

	gsutil cp gs://bucket1q2w/jpliu/titancna/tcga/coad/${optimal} ${SAMPLE}/

	solution=${SAMPLE}/$(basename ${optimal})
	python /home/lji226/phylowgs/parser/parse_cnvs.py -f titan -c ${cellularity} \
	--cnv-output ${SAMPLE}/cnv.txt ${solution}

	python /home/lji226/phylowgs/parser/create_phylowgs_inputs.py --cnv sample1=${SAMPLE}/cnv.txt \
	--vcf-type sample1=mutect_smchet sample1=${VCFDIR}/${SAMPLE:0:12}.filtered.vcf.gz --output-cnvs ${SAMPLE}/cnv_data.txt --output-variants ${SAMPLE}/ssm_data.txt
	echo $(wc -l ${SAMPLE}/cnv_data.txt)
done